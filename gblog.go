package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/russross/blackfriday"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"time"
)

type Site struct {
	Site     template.URL
	Articles Articles
}

type Meta struct {
	Title   string
	Link    template.URL
	ModTime string
	Time    time.Time
}

type Article struct {
	Meta
	Contents template.HTML
}

type Articles []*Article

func (a Articles) Len() int {
	return len(a)
}
func (a Articles) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a Articles) Less(i, j int) bool {
	return a[i].Time.Before(a[j].Time)
}

var article map[string]*Article
var articles Articles

var tpl *template.Template

func init() {
	article = make(map[string]*Article)
	articles = Articles{}
	dir, err := ioutil.ReadDir("articles")
	if err != nil {
		panic(err)
	}
	for _, info := range dir {
		contents, err := ioutil.ReadFile("articles/" + info.Name())
		if err != nil {
			panic(err)
		}
		buf := bytes.NewBuffer(contents)
		dec := json.NewDecoder(buf)
		meta := new(Meta)
		dec.Decode(meta)
		f := "2006-1-2 15:04:05 MST"
		meta.Time, err = time.Parse(f, meta.ModTime)
		if err != nil {
			continue
		}
		first, err := ioutil.ReadAll(dec.Buffered())
		if err != nil {
			panic(err)
		}
		rest, err := ioutil.ReadAll(buf)
		src := []byte(string(first) + string(rest))
		md := blackfriday.MarkdownCommon(src)
		a := &Article{*meta, template.HTML(md)}
		article[string(meta.Link)] = a
		articles = append(articles, a)
	}
	sort.Sort(sort.Reverse(articles))
	t, err := template.ParseFiles("templates/article.tpl")
	if err != nil {
		panic(err)
	}
	tpl = t
	m := mux.NewRouter()
	m.HandleFunc("/", home)
	m.HandleFunc("/{article}", page)
	http.Handle("/stylesheets/", http.FileServer(http.Dir("public")))
	http.Handle("/favicon.ico", http.FileServer(http.Dir("public")))
	http.Handle("/", m)
}

func main() {
	host := flag.String("host", "localhost:8080", "Host to listen on")
	flag.Parse()
	fmt.Println("Starting on", *host)
	log.Fatal(http.ListenAndServe(*host, nil))
}

func home(w http.ResponseWriter, r *http.Request) {
	as := Articles{}
	for _, a := range articles {
		b := *a
		b.Link = "http://" + template.URL(r.Host) + b.Link
		as = append(as, &b)
	}
	err := tpl.Execute(w, Site{Site: "http://" + template.URL(r.Host), Articles: as})
	if err != nil {
		panic(err)
	}
}

func page(w http.ResponseWriter, r *http.Request) {
	aname := mux.Vars(r)["article"]
	a := article[aname]
	if a == nil {
		http.Error(w, "Not Found", 404)
		return
	}
	b := *a
	b.Link = "http://" + template.URL(r.Host+"/"+string(b.Link))
	err := tpl.Execute(w, Site{Site: "http://" + template.URL(r.Host), Articles: Articles{&b}})
	if err != nil {
		panic(err)
	}
}
