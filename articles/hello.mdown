{
    "Title": "Bible Verse Of The Day",
    "Link": "romans6.23",
    "ModTime": "2013-06-08 14:29:00 CDT"
}

> What shall we say then? Shall we continue in sin,
> that grace may abound?

> God forbid. How shall we that are dead to sin live any longer therein?

[Romans 6.1-2 Authorized Version](http://www.blueletterbible.org/Bible.cfm?b=Rom&c=6&v=1&t=KJV#top)
