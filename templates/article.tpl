<!doctype html>
<html>
  <head>
    <link rel="stylesheet" href="/stylesheets/style.css" media="screen">
    <link rel="stylesheet" href="/stylesheets/print.css" media="print">
    <title>Against Copyleft</title>
  </head>
  <body>
    <h1 id="main"><a href="{{.Site}}">Against Copyleft</a></h1>
    {{range .Articles}}
      <div class="post">
        <h1><a href="{{.Link}}">{{.Title}}</a></h1>
        <div class="date">{{.Time}}</div>
        <div class="contents">
	  {{.Contents}}
        </div>
      </div>
    {{end}}
  </body>
</html>
